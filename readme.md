# 1.create repo
   cd ~/your-project-folder cd เข้าไปในโฟลเดอร์โปรเจค
 git init  สร้าง git repo ขึ้นมาก่อนด้วยคำสั่ง
  git add .  stage ไฟล์ทั้งหมดเพื่อจะส่งไปยัง remote   (ถ้าใครจะเอาบางไฟล์ก็ให้เลือก add ทีละไฟล์ก็เปลี่ยน . เป็นชื่อไฟล์หรือ * เอานะครับ)
 git commit -m "Initial commit"  Commit ไฟล์ชุดแรกที่ Stage เอาไว้ ตั้งชื่อตามต้องการเลย
 สร้าง Remote repo และเพิ่มไว้ในตั้งค่าไว้เพื่อใช้ push ต่อไป
git remote add origin [email protected]:your-username/your-repository.git
 สุดท้ายก็ push ไฟล์ที่ Commit ไว้ไปยัง Remote ครับ
  git push -u origin master
 (มันจะถาม username กับ password ก็ใส่ให้ถูกต้องนะครับ)


# 2.git ignore
 สามารถสร้างได้ 2 วิธี 
   วิธที่ 1 โดยใช้ git bash แล้วใช้คำสั้ง touch .gitignore 
   วิธีืที่ 2 ทำการ คลิกขวาแล้วส้ราง file ชื่อ .gitignore
   โดย .gitignore มีหน้าที่ตรวจสอบว่าเราไม่ต้องการ file ไหนที่ไม่ต้องการเอาขึ้นไปใน git  
   ยกตัวอย่างเช่น ใน file .gitignore
       node_modules
       /dist
   นั้นหมายความว่า เมื่อเราทำการ push ขึ้น git จะไม่นำ file node_modules และ dist ขึ้นไปบน git ด้วย

# 3.commit
 เมื่อเราแก้ไข หรือ เพิ่มไฟล์ ไฟล์นั้นจะมีสภาพเป็น unstaged 
 ต้อง staged ไฟล์ที่ต้องการก่อน เพื่อให้ git track ไฟล์นั้น

 ใช้คำสั่ง git commit เพื่อ commit ไว้ใน local
 ควร comment ให้มีเนื้อหาที่ตรงกับสิ่งที่ทำ

# 4.branch & merge & solve conflict 
 การ merge คือการรวมส่วนต่างของแต่ละ commit ของ brunch อื่นๆ มาไว้รวมกัน อาจะมีการร้องขอการ merge request เพื่อขอ permission 
  ในกรณีที่มีการแก้ไข source code  ที่เดียวกันแต่ละไฟล์ แล้วมา merge เข้าด้วยกัน git จะแจ้งเตือนว่ามีการแก้ไขเกิดขึ้นพร้อมกัน 2 จุด แล้ว git ไม่สามารถตัดสินใจแทนให้ได้ว่าจะใช้ source code ของใคร เราต้อง solve ด้วยตัวเอง มีทางแก้หลายวิธี อาจจะเลือกของใครสักคน หรือ รวมกัน หรือ ทำการเรียกประชุมเพื่อแนวทางแล้วแก้ไขแล้ว ทำการ merge ใหม่

# 5.tag  
 เราสามารถ tag เพื่อบอก version
